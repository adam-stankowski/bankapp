package as.bankapp.service;

import as.bankapp.model.Account;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-config.xml")
public class AmountAdjusterServiceImplTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Autowired
    private AmountAdjusterService amountAdjusterService;

    private Account testSource, testDestination;

    @Before
    public void setUp() {
        testSource = new Account("123", BigDecimal.valueOf(1000));
        testDestination = new Account("456", BigDecimal.valueOf(2000));
    }

    @Test
    public void whenAnyAccountNullThenException() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Source and Destination accounts");
        amountAdjusterService.adjustAmounts(null, null, BigDecimal.ONE);
    }

    @Test
    public void whenAmountZeroOrNullThenException() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Transfer amount cannot");
        amountAdjusterService.adjustAmounts(testSource, testDestination, null);

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Transfer amount cannot");
        amountAdjusterService.adjustAmounts(testSource, testDestination, BigDecimal.ZERO);
    }

    @Test
    public void whenTransferAmountThenSaldosUpdated() {
        amountAdjusterService.adjustAmounts(testSource, testDestination, BigDecimal.valueOf(500));
        MatcherAssert.assertThat(testSource.getSaldo(), Matchers.comparesEqualTo(BigDecimal.valueOf(500)));
        MatcherAssert.assertThat(testDestination.getSaldo(), Matchers.comparesEqualTo(BigDecimal.valueOf(2500)));
    }
}
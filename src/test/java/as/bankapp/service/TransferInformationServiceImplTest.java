package as.bankapp.service;

import as.bankapp.model.Account;
import as.bankapp.model.TransferInformation;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.hasSize;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-config.xml")
public class TransferInformationServiceImplTest {

    @Autowired
    TransferInformationService transferInformationService;

    private Account testSource, testDestination;

    @Before
    public void setUp() throws Exception {
        testSource = new Account("123", BigDecimal.valueOf(1000));
        testDestination = new Account("456", BigDecimal.valueOf(2000));
    }

    @Test
    public void whenTransferAmountThenOperationAdded() {
        transferInformationService.updateTransferInformation(testSource, testDestination, BigDecimal.valueOf(500));
        MatcherAssert.assertThat(testSource.getTransferInformation(), hasSize(1));
        MatcherAssert.assertThat(testDestination.getTransferInformation(), hasSize(1));
    }

    @Test
    public void whenTransferAmountThenOperationsAmountsAndSourcesCorrect() {
        BigDecimal transferAmount = BigDecimal.valueOf(500);
        transferInformationService.updateTransferInformation(testSource, testDestination, transferAmount);
        TransferInformation sourceOperation = testSource.getTransferInformation().get(0);
        TransferInformation destinationOperation = testDestination.getTransferInformation().get(0);

        MatcherAssert.assertThat(sourceOperation.getAmount(), Matchers.comparesEqualTo(transferAmount.negate()));
        MatcherAssert.assertThat(destinationOperation.getAmount(), Matchers.comparesEqualTo(transferAmount));
        MatcherAssert.assertThat(sourceOperation.getFromAccountNumber(), Matchers.is(testSource.getAccountNumber()));
        MatcherAssert.assertThat(sourceOperation.getToAccountNumber(), Matchers.is(testDestination.getAccountNumber()));
        MatcherAssert.assertThat(destinationOperation.getFromAccountNumber(), Matchers.is(testSource.getAccountNumber()));
        MatcherAssert.assertThat(destinationOperation.getToAccountNumber(), Matchers.is(testDestination.getAccountNumber()));
    }

    @Test
    public void whenTransferingThenSourceTransferInformationReturned() {
        BigDecimal amount = BigDecimal.valueOf(500);
        TransferInformation transferInformation = transferInformationService.updateTransferInformation(testSource, testDestination, amount);
        MatcherAssert.assertThat(transferInformation.getAmount(), Matchers.comparesEqualTo(amount.negate()));
        MatcherAssert.assertThat(transferInformation.getFromAccountNumber(), Matchers.is(testSource.getAccountNumber()));
        MatcherAssert.assertThat(transferInformation.getToAccountNumber(), Matchers.is(testDestination.getAccountNumber()));
    }
}
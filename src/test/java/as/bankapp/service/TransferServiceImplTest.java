package as.bankapp.service;

import as.bankapp.dao.AccountDao;
import as.bankapp.model.Account;
import as.bankapp.model.TransferInformation;
import as.bankapp.viewmodel.TransferRequest;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

public class TransferServiceImplTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private AmountAdjusterService amountAdjusterService;
    private TransferInformationService transferInformationService;
    private TransferService transferService;
    private AccountDao accountDao;

    private Account testSource, testDestination;
    private TransferRequest transferRequest;

    @Before
    public void setUp() {
        amountAdjusterService = Mockito.mock(AmountAdjusterService.class);
        transferInformationService = Mockito.mock(TransferInformationService.class);
        accountDao = Mockito.mock(AccountDao.class);
        transferService = new TransferServiceImpl(amountAdjusterService, transferInformationService, accountDao);

        testSource = new Account("123", BigDecimal.valueOf(1000));
        testDestination = new Account("456", BigDecimal.valueOf(2000));
        transferRequest = new TransferRequest(testSource.getAccountNumber(), testDestination.getAccountNumber(), BigDecimal.valueOf(500));
        Mockito.when(accountDao.getAccountByNumber(testSource.getAccountNumber())).thenReturn(Optional.of(testSource));
        Mockito.when(accountDao.getAccountByNumber(testDestination.getAccountNumber())).thenReturn(Optional.of(testDestination));
    }


    @Test
    public void whenTransferAmountThenTransferServiceFired() {
        transferService.transfer(transferRequest);
        Mockito.verify(amountAdjusterService, Mockito.times(1)).adjustAmounts(testSource, testDestination, transferRequest.getAmount());
    }

    @Test
    public void whenTransferAmountThenOperationServiceFired() {
        BigDecimal amount = BigDecimal.valueOf(500);
        transferService.transfer(transferRequest);
        Mockito.verify(transferInformationService, Mockito.times(1)).updateTransferInformation(testSource, testDestination, transferRequest.getAmount());
    }

    @Test
    public void whenTransferAmountThenProperTransferInformation() {
        TransferInformation testInformation = new TransferInformation(LocalDateTime.now(), testSource.getAccountNumber(), testDestination.getAccountNumber(), transferRequest.getAmount().negate());
        Mockito.when(transferInformationService.updateTransferInformation(testSource, testDestination, transferRequest.getAmount())).thenReturn(testInformation);
        TransferInformation transfer = transferService.transfer(transferRequest);
        MatcherAssert.assertThat(transfer.getAmount(), Matchers.comparesEqualTo(transferRequest.getAmount().negate()));
        MatcherAssert.assertThat(transfer.getFromAccountNumber(), Matchers.is(testSource.getAccountNumber()));
        MatcherAssert.assertThat(transfer.getToAccountNumber(), Matchers.is(testDestination.getAccountNumber()));
    }
}
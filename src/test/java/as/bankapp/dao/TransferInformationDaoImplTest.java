package as.bankapp.dao;

import as.bankapp.model.TransferInformation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;

@RunWith(MockitoJUnitRunner.class)
public class TransferInformationDaoImplTest {

    @Mock
    private EntityManager entityManager;

    @InjectMocks
    private TransferInformationDao transferInformationDao = new TransferInformationDaoImpl();

    @Before
    public void setUp() {
    }

    @Test
    public void whenFindByAccountNumber_thenGetResultListCalled() {
        String accountNumber = "123456";
        TypedQuery<TransferInformation> query = Mockito.mock(TypedQuery.class);
        Mockito.when(entityManager.createNamedQuery(anyString(), eq(TransferInformation.class))).thenReturn(query);
        List<TransferInformation> result  = transferInformationDao.findByAccountNumber(accountNumber);
        Mockito.verify(query, Mockito.times(1)).getResultList();
    }
}
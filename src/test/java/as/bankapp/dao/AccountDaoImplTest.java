package as.bankapp.dao;

import as.bankapp.model.Account;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-config.xml")
public class AccountDaoImplTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Autowired
    private AccountDao accountDao;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void whenFromEmptyOrNullThenException() {
        expectedException.expect(IllegalArgumentException.class);
        accountDao.getAccountByNumber("");
        expectedException.expect(IllegalArgumentException.class);
        accountDao.getAccountByNumber(null);
    }

    @Test
    public void whenAccountExistsThenReturned() {
        String accountNumber = "123";
        accountDao.addAccount(accountNumber, BigDecimal.ZERO);
        Optional<Account> account = accountDao.getAccountByNumber(accountNumber);
        MatcherAssert.assertThat(account.isPresent(), Matchers.is(true));
        MatcherAssert.assertThat(account.get().getAccountNumber(), Matchers.is(accountNumber));
    }

    @Test
    public void whenAccountNotExistsThenEmpty() {
        String accountNumber = "123";
        accountDao.addAccount(accountNumber, BigDecimal.ZERO);
        Optional<Account> account = accountDao.getAccountByNumber("23445");
        MatcherAssert.assertThat(account.isPresent(), Matchers.is(false));
    }
}
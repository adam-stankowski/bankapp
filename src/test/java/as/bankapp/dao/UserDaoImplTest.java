package as.bankapp.dao;

import as.bankapp.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Optional;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;

@RunWith(MockitoJUnitRunner.class)
public class UserDaoImplTest {

    @Mock
    private EntityManager entityManager;

    @InjectMocks
    private UserDao userDao = new UserDaoImpl();
    private TypedQuery<User> typedQuery;
    private User testUser;


    @Before
    public void setUp() {
        testUser = new User();
        testUser.setUsername("adam");
        testUser.setPassword("password");
    }

    @Test
    public void whenUsernameNotExists_thenEmptyOptional() {
        typedQuery = Mockito.mock(TypedQuery.class);
        Mockito.when(typedQuery.getSingleResult()).thenThrow(new NoResultException());
        Mockito.when(entityManager.createNamedQuery(anyString(), eq(User.class))).thenReturn(typedQuery);

        Optional<User> result = userDao.findByUsername("anyUsername");
        Assert.assertFalse(result.isPresent());
    }

    @Test
    public void whenUsernameExists_thenUserReturned() {
        typedQuery = Mockito.mock(TypedQuery.class);
        Mockito.when(typedQuery.getSingleResult()).thenReturn(testUser);
        Mockito.when(entityManager.createNamedQuery(anyString(), eq(User.class))).thenReturn(typedQuery);

        Optional<User> result = userDao.findByUsername("adam");
        Assert.assertTrue(result.isPresent());
    }
}
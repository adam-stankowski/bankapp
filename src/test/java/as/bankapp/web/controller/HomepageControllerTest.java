package as.bankapp.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class HomepageControllerTest {
    private HomepageController homepageController;

    @Before
    public void setUp() {
        homepageController = new HomepageController();
    }

    @Test
    public void whenRequestingHomeThenHomeViewReturned() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(homepageController).build();
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(MockMvcResultMatchers.view().name("home"));
    }
}
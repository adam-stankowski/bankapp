package as.bankapp.web.controller;

import as.bankapp.dao.TransferInformationDao;
import as.bankapp.exception.AccountUnavailableException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class AccountHistoryControllerTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private TransferInformationDao transferInformationDao;
    private AccountHistoryController controller;

    @Before
    public void setUp() {
        transferInformationDao = Mockito.mock(TransferInformationDao.class);
        controller = new AccountHistoryController(transferInformationDao);
    }

    @Test
    public void whenHistoryRequested_thenHistoryViewServed() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        mockMvc.perform(MockMvcRequestBuilders.get("/history"))
                .andExpect(MockMvcResultMatchers.view().name("historyForm"));
    }

    @Test
    public void whenHistoryFormSubmitted_thenHistoryDetailsViewServed() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        mockMvc.perform(MockMvcRequestBuilders.post("/history").param("accountNumber", "123"))
                .andExpect(MockMvcResultMatchers.view().name("historyDetails"));
    }

    @Test
    public void havingNoTransfersForAccount_whenHistoryFormSubmitted_thenHistoryDetailsViewServed() throws Exception {
        String accountNumber = "123";
        Mockito.when(transferInformationDao.findByAccountNumber(accountNumber)).thenThrow(new AccountUnavailableException());
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        expectedException.expect(AccountUnavailableException.class);
        mockMvc.perform(MockMvcRequestBuilders.post("/history").param("accountNumber", accountNumber))
                .andExpect(MockMvcResultMatchers.view().name("historyDetails"));
    }
}
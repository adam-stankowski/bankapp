package as.bankapp.web.controller;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import as.bankapp.model.TransferInformation;
import as.bankapp.service.TransferInformationService;
import as.bankapp.service.TransferService;

import static org.mockito.Matchers.anyObject;

public class TransferControllerTest {

    private TransferController transferController;
    private TransferService transferService;
    private TransferInformationService transferInformationService;
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        transferService = Mockito.mock(TransferService.class);
        transferInformationService = Mockito.mock(TransferInformationService.class);
        transferController = new TransferController(transferService, transferInformationService);
        mockMvc = MockMvcBuilders.standaloneSetup(transferController).build();
    }

    @Test
    public void whenGetRequestTransferThenTransferFormView() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/transfer"))
                .andExpect(MockMvcResultMatchers.view().name("transferForm"));
    }

    @Test
    public void whenPostRequestTransferThenRedirectToTransferSuccessView() throws Exception {
        TransferInformation resultTransferInformation = new TransferInformation(LocalDateTime.now(), "123", "456", BigDecimal.valueOf(34));
        Mockito.when(transferService.transfer(anyObject())).thenReturn(resultTransferInformation);
        mockMvc.perform(MockMvcRequestBuilders.post("/transfer")
                .param("accountFromNumber", "123")
                .param("accountToNumber", "345")
                .param("amount", "120"))
                .andExpect(MockMvcResultMatchers.redirectedUrl(String.format("/transfer/success/%d", resultTransferInformation.getId())));
    }

    @Test
    public void whenPostRequestTransferWithErrorsThenValidationAndComeBackToFormView() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/transfer")
                .param("accountFromNumber", "1")
                .param("accountToNumber", "345")
                .param("amount", "0"))
                .andExpect(MockMvcResultMatchers.view().name("transferForm"))
                .andExpect(MockMvcResultMatchers.model().hasErrors())
                .andExpect(MockMvcResultMatchers.model().attributeHasFieldErrors("transferRequest", "accountFromNumber", "amount"));
    }
}
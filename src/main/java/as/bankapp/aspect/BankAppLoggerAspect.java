package as.bankapp.aspect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class BankAppLoggerAspect {

    @Pointcut("@annotation(as.bankapp.annotation.LogDebug)")
    public void logDebugAnnotation() {
    }

    @Pointcut("within(as.bankapp.service.*)")
    public void anyServiceMethod(){
    }

    @Before("logDebugAnnotation()")
    public void logDebugEntry(JoinPoint joinPoint) {

        Logger logger = LogManager.getLogger(joinPoint.getTarget().getClass().getSimpleName());
        logger.debug(String.format("%s params %s",joinPoint.getSignature().getName(), Arrays.toString(joinPoint.getArgs())));
    }

    @AfterThrowing(value = "anyServiceMethod()", throwing = "ex")
    public void logException(JoinPoint joinPoint, Throwable ex){
        Logger logger = LogManager.getLogger(joinPoint.getTarget().getClass().getSimpleName());
        logger.error("Exception thrown", ex);
    }
}

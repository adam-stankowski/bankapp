package as.bankapp.viewmodel;

import java.math.BigDecimal;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TransferRequest {
    @NotNull
    @Size(min = 2, max = 10, message = "{transferRequest.accountFromNumber.size}")
    private String accountFromNumber;
    @NotNull
    @Size(min = 2, max = 10, message = "{transferRequest.accountToNumber.size}")
    private String accountToNumber;
    @NotNull
    @DecimalMin(value = "0.01", message = "{transferRequest.amount.value}")
    private BigDecimal amount;

    public TransferRequest() {
        accountFromNumber = "";
        accountToNumber = "";
        amount = BigDecimal.ZERO;
    }

    public TransferRequest(String accountFromNumber, String accountToNumber, BigDecimal amount) {
        this.accountFromNumber = accountFromNumber;
        this.accountToNumber = accountToNumber;
        this.amount = amount;
    }

    public String getAccountFromNumber() {
        return accountFromNumber;
    }

    public String getAccountToNumber() {
        return accountToNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAccountFromNumber(String accountFromNumber) {
        this.accountFromNumber = accountFromNumber;
    }

    public void setAccountToNumber(String accountToNumber) {
        this.accountToNumber = accountToNumber;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}

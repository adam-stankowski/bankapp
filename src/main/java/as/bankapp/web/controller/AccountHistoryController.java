package as.bankapp.web.controller;

import as.bankapp.dao.TransferInformationDao;
import as.bankapp.model.TransferInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/history")
public class AccountHistoryController {
    private final TransferInformationDao transferInformationDao;

    @Autowired
    public AccountHistoryController(TransferInformationDao transferInformationDao) {
        this.transferInformationDao = transferInformationDao;
    }

    @GetMapping
    public String showHistoryForm(Model model) {
        model.addAttribute("transfers", new ArrayList<TransferInformation>());
        return "historyForm";
    }

    @PostMapping
    public String showHistoryDetails(Model model, @RequestParam("accountNumber") String accountNumber) {
        List<TransferInformation> transfersForAccount = transferInformationDao.findByAccountNumber(accountNumber);
        model.addAttribute("transfers", transfersForAccount);
        return "historyDetails";
    }
}

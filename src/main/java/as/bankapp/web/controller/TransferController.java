package as.bankapp.web.controller;

import as.bankapp.model.TransferInformation;
import as.bankapp.service.TransferInformationService;
import as.bankapp.service.TransferService;
import as.bankapp.viewmodel.TransferRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/transfer")
public class TransferController {

    private final TransferService transferService;
    private final TransferInformationService transferInformationService;

    public TransferController(TransferService transferService, TransferInformationService transferInformationService) {
        this.transferService = transferService;
        this.transferInformationService = transferInformationService;
    }

    @GetMapping
    public String displayTransferForm(Model model) {
        model.addAttribute(new TransferRequest());
        return "transferForm";
    }
    @PostMapping
    public String acceptTransferForm(@Valid TransferRequest transferRequest, Errors errors) {
        if (errors.hasErrors()) {
            return "transferForm";
        }

        TransferInformation transferInformation = transferService.transfer(transferRequest);
        return String.format("redirect:/transfer/success/%d", transferInformation.getId());
    }

    @RequestMapping(value = "/success/{id}", method = RequestMethod.GET)
    public String displaySuccessPage(@PathVariable long id, Model model) {
        TransferInformation transferInformation = transferInformationService.findByIdOrThrow(id);
        model.addAttribute("transferInformation", transferInformation);
        return "transferSuccess";
    }
}

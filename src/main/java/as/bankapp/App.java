package as.bankapp;


import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import javax.servlet.ServletException;
import java.io.File;
import java.io.IOException;

public class App {
    private static final int PORT = 8800;

    public static void main(String[] args) throws ServletException, LifecycleException {
        String appBase = "web";
        Tomcat tomcat = new Tomcat();
        tomcat.setBaseDir(createTempDir());
        tomcat.setPort(PORT);
        tomcat.getHost().setAppBase(appBase);
        tomcat.getConnector();
        tomcat.addWebapp("/", new File(appBase).getAbsolutePath());
        tomcat.start();
        tomcat.getServer().await();
    }

    private static String createTempDir() {

        try {
            File tempDir = File.createTempFile("tomcat.", "." + PORT);
            tempDir.delete();
            tempDir.mkdir();
            tempDir.deleteOnExit();
            return tempDir.getAbsolutePath();
        } catch (IOException ex) {
            throw new RuntimeException("Unable to create tempDir. java.io.tmpdir is set to "
                    + System.getProperty("java.io.tmpdir"),ex);
        }
    }

}

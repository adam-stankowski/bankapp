package as.bankapp.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Account {
    private final String accountNumber;
    private BigDecimal saldo;
    private final List<TransferInformation> transferInformation;

    public Account(String accountNumber, BigDecimal saldo) {
        this.accountNumber = accountNumber;
        this.saldo = saldo;
        transferInformation = new ArrayList<>();
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public List<TransferInformation> getTransferInformation() {
        return new ArrayList<>(transferInformation);
    }

    public void addAmount(BigDecimal amount) {
        saldo = saldo.add(amount);
    }

    public void subtractAmount(BigDecimal amount) {
        saldo = saldo.subtract(amount);
    }

    public void addOperation(TransferInformation operation) {
        this.transferInformation.add(operation);
    }
}

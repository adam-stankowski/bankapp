package as.bankapp.model;

public interface PersistableModel {
    void setId(long id);
    long getId();
}

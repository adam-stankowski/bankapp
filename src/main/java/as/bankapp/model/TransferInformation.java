package as.bankapp.model;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@NamedQueries(
        @NamedQuery(name = "TransferInformation.findByAccountNumber", query = "select ti from TransferInformation ti where ti.fromAccountNumber = :accountNumber or ti.toAccountNumber = :accountNumber")
)
public class TransferInformation extends AbstractPersistableModel {
    private LocalDateTime operationDate;
    private String fromAccountNumber;
    private String toAccountNumber;
    private BigDecimal amount;

    public TransferInformation() {
    }

    public TransferInformation(LocalDateTime operationDate, String fromAccountNumber, String toAccountNumber, BigDecimal amount) {
        this.operationDate = operationDate;
        this.fromAccountNumber = fromAccountNumber;
        this.toAccountNumber = toAccountNumber;
        this.amount = amount;
    }

    public LocalDateTime getOperationDate() {
        return operationDate;
    }

    public String getFromAccountNumber() {
        return fromAccountNumber;
    }

    public String getToAccountNumber() {
        return toAccountNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setOperationDate(LocalDateTime operationDate) {
        this.operationDate = operationDate;
    }

    public void setFromAccountNumber(String fromAccountNumber) {
        this.fromAccountNumber = fromAccountNumber;
    }

    public void setToAccountNumber(String toAccountNumber) {
        this.toAccountNumber = toAccountNumber;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}

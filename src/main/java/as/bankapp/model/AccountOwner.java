package as.bankapp.model;

import as.bankapp.enums.OwnerType;

import java.util.ArrayList;
import java.util.List;

public class AccountOwner {
    private final String name;
    private final OwnerType type;
    private final List<Account> accounts;

    public AccountOwner(String name, OwnerType type) {
        this.name = name;
        this.type = type;
        this.accounts = new ArrayList<>();
    }
}

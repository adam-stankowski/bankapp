package as.bankapp.service;

import as.bankapp.dao.UserDao;
import as.bankapp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class UserAuthenticationService implements UserDetailsService {

    private static final String USERNAME_OR_PASSWORD_INVALID = "Username or password is invalid";
    private final UserDao userDao;

    @Autowired
    public UserAuthenticationService(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(USERNAME_OR_PASSWORD_INVALID));

        return mapToUserDetails(user);
    }

    private UserDetails mapToUserDetails(User user) {
        return new org.springframework.security.core.userdetails.User("user.getUsername()", "user.getPassword()", null);
    }
}

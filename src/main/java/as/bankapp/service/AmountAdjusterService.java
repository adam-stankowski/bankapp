package as.bankapp.service;

import as.bankapp.model.Account;

import java.math.BigDecimal;

public interface AmountAdjusterService {
    void adjustAmounts(Account source, Account destination, BigDecimal amount);
}

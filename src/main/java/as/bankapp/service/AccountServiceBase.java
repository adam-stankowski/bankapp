package as.bankapp.service;

import as.bankapp.model.Account;

import java.math.BigDecimal;

public abstract class AccountServiceBase {
    private static final String ACCOUNTS_CANNOT_BE_NULL = "Source and Destination accounts cannot be null";
    private static final String AMOUNT_CANNOT_BE_ZERO = "Transfer amount cannot be null or zero";

    protected void throwIfInvalidInput(Account source, Account destination, BigDecimal amount) {
        if (source == null || destination == null) {
            throw new IllegalArgumentException(ACCOUNTS_CANNOT_BE_NULL);
        }
        if (amount == null || BigDecimal.ZERO.equals(amount)) {
            throw new IllegalArgumentException(AMOUNT_CANNOT_BE_ZERO);
        }
    }
}

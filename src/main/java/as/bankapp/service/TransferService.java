package as.bankapp.service;

import as.bankapp.model.TransferInformation;
import as.bankapp.viewmodel.TransferRequest;

public interface TransferService {
    TransferInformation transfer(TransferRequest transferRequest);
}

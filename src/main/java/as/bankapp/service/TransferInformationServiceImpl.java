package as.bankapp.service;

import as.bankapp.annotation.LogDebug;
import as.bankapp.dao.TransferInformationDao;
import as.bankapp.model.Account;
import as.bankapp.model.TransferInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

@Component
public class TransferInformationServiceImpl extends AccountServiceBase implements TransferInformationService {

    private final TransferInformationDao transferInformationDao;

    @Autowired
    public TransferInformationServiceImpl(TransferInformationDao transferInformationDao) {
        this.transferInformationDao = transferInformationDao;
    }

    @Override
    @LogDebug
    public Optional<TransferInformation> findById(long id) {
        return transferInformationDao.findById(id);
    }

    @Override
    @LogDebug
    public TransferInformation findByIdOrThrow(long id) {
        return transferInformationDao.findByIdOrThrow(id);
    }

    @Override
    @LogDebug
    public TransferInformation updateTransferInformation(Account source, Account destination, BigDecimal amount) {
        LocalDateTime operationDate = LocalDateTime.now();
        TransferInformation sourceOperation = new TransferInformation(operationDate, source.getAccountNumber(), destination.getAccountNumber(), amount.negate());
        TransferInformation destinationOperation = new TransferInformation(operationDate, source.getAccountNumber(), destination.getAccountNumber(), amount);
        source.addOperation(sourceOperation);
        destination.addOperation(destinationOperation);
        return transferInformationDao.save(sourceOperation);
    }
}

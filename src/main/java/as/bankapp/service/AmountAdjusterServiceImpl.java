package as.bankapp.service;

import as.bankapp.annotation.LogDebug;
import as.bankapp.model.Account;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class AmountAdjusterServiceImpl extends AccountServiceBase implements AmountAdjusterService {

    @Override
    @LogDebug
    public void adjustAmounts(Account source, Account destination, BigDecimal amount) {
        throwIfInvalidInput(source, destination, amount);

        source.subtractAmount(amount);
        destination.addAmount(amount);
    }


}

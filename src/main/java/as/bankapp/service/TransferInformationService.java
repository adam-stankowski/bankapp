package as.bankapp.service;

import as.bankapp.model.Account;
import as.bankapp.model.TransferInformation;

import java.math.BigDecimal;
import java.util.Optional;

public interface TransferInformationService {
    Optional<TransferInformation> findById(long id);
    TransferInformation findByIdOrThrow(long id);

    TransferInformation updateTransferInformation(Account source, Account destination, BigDecimal amount);
}

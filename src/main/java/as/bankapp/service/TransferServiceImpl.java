package as.bankapp.service;

import as.bankapp.dao.AccountDao;
import as.bankapp.model.Account;
import as.bankapp.model.TransferInformation;
import as.bankapp.viewmodel.TransferRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class TransferServiceImpl extends AccountServiceBase implements TransferService {

    private final AmountAdjusterService amountAdjusterService;
    private final TransferInformationService transferInformationService;
    private final AccountDao accountDao;

    @Autowired
    public TransferServiceImpl(AmountAdjusterService amountAdjusterService, TransferInformationService transferInformationService,
                               AccountDao accountDao) {
        this.amountAdjusterService = amountAdjusterService;
        this.transferInformationService = transferInformationService;
        this.accountDao = accountDao;
    }

    @Override
    public TransferInformation transfer(TransferRequest transferRequest) {
        Account accountFrom = accountDao.getAccountByNumber(transferRequest.getAccountFromNumber())
                .orElseThrow(()-> new IllegalArgumentException(String.format("Account with number %s does not exist", transferRequest.getAccountFromNumber())));
        Account accountTo = accountDao.getAccountByNumber(transferRequest.getAccountToNumber())
                .orElseThrow(()-> new IllegalArgumentException(String.format("Account with number %s does not exist", transferRequest.getAccountToNumber())));

        return performTransfer(accountFrom, accountTo, transferRequest.getAmount());
    }

    private TransferInformation performTransfer(Account source, Account destination, BigDecimal amount) {
        throwIfInvalidInput(source, destination, amount);

        amountAdjusterService.adjustAmounts(source, destination, amount);
        return transferInformationService.updateTransferInformation(source, destination, amount);
    }
}

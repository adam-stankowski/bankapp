package as.bankapp.exception;

public class AccountUnavailableException extends RuntimeException {
    public AccountUnavailableException() {
    }

    public AccountUnavailableException(String message) {
        super(message);
    }
}

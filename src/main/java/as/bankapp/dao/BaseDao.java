package as.bankapp.dao;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface BaseDao<T> {
    Optional<T> findById(long id);

    T findByIdOrThrow(long id);

    T save(T item);

    List<T> namedQueryAsList(String namedQuery, Map<String, Object> params);

    Optional<T> namedQueryAsSingleResultOptional(String namedQuery, Map<String, Object> params);
}

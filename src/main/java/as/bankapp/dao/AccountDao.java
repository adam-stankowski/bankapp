package as.bankapp.dao;

import as.bankapp.model.Account;

import java.math.BigDecimal;
import java.util.Optional;

public interface AccountDao {

    Optional<Account> getAccountByNumber(String accountFromNumber);
    void addAccount(String accountNumber, BigDecimal saldo);
}

package as.bankapp.dao;

import as.bankapp.model.User;

import java.util.Optional;

public interface UserDao extends BaseDao<User> {
    Optional<User> findByUsername(String username);
}

package as.bankapp.dao;

import as.bankapp.model.User;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class UserDaoImpl extends AbstractBaseDao<User> implements UserDao {

    public UserDaoImpl() {
        super(User.class);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        Map<String, Object> params = new HashMap<>();
        params.put("username", username);
        return namedQueryAsSingleResultOptional("User.findByUsername", params);
    }
}

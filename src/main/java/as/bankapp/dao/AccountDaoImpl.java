package as.bankapp.dao;

import as.bankapp.model.Account;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class AccountDaoImpl implements AccountDao {
    private Map<String, Account> accounts;

    public AccountDaoImpl() {
        accounts = new HashMap<>();
    }

    @Override
    public Optional<Account> getAccountByNumber(String accountNumber) {
        validateAccountNumberOrThrow(accountNumber);

        return Optional.ofNullable(accounts.get(accountNumber));
    }

    @Override
    public void addAccount(String accountNumber, BigDecimal saldo) {
        validateAccountNumberOrThrow(accountNumber);

        accounts.put(accountNumber, new Account(accountNumber, saldo));
    }

    private void validateAccountNumberOrThrow(String accountNumber) {
        if(accountNumber == null || "".equals(accountNumber)){
            throw new IllegalArgumentException("Account number cannot be null or empty");
        }
    }
}

package as.bankapp.dao;

import as.bankapp.model.TransferInformation;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TransferInformationDaoImpl extends AbstractBaseDao<TransferInformation> implements TransferInformationDao {

    private static final String ACCOUNT_NUMBER_INVALID = "Account number %s is invalid";

    public TransferInformationDaoImpl() {
        super(TransferInformation.class);
    }

    @Override
    public List<TransferInformation> findByAccountNumber(String accountNumber) {
        Map<String, Object> params = new HashMap<>();
        params.put("accountNumber", accountNumber);
        return namedQueryAsList("TransferInformation.findByAccountNumber", params);
    }
}

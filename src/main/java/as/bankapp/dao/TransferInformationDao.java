package as.bankapp.dao;

import as.bankapp.model.TransferInformation;

import java.util.List;

public interface TransferInformationDao extends BaseDao<TransferInformation>  {
    List<TransferInformation> findByAccountNumber(String accountNumber);
}

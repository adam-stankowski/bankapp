package as.bankapp.dao;

import as.bankapp.model.PersistableModel;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractBaseDao<T extends PersistableModel> implements BaseDao<T> {
    private static final String COULD_NOT_FIND_ITEM = "Could not find %s with id %d";

    @PersistenceContext
    private EntityManager entityManager;
    private Class<T> entityType;


    public AbstractBaseDao(Class<T> entityType) {
        this.entityType = entityType;
    }

    @Override
    public Optional<T> findById(long id) {
        return Optional.ofNullable(entityManager.find(entityType, id));
    }

    @Override
    public T findByIdOrThrow(long id) {
        return findById(id).orElseThrow(() -> new IllegalArgumentException(String.format(COULD_NOT_FIND_ITEM, this.getClass().getSimpleName(), id)));
    }

    @Override
    public T save(T item) {
        entityManager.persist(item);
        return item;
    }

    @Override
    public List<T> namedQueryAsList(String namedQuery, Map<String, Object> params) {
        TypedQuery<T> query = createParametrizedQuery(namedQuery, params);
        return query.getResultList();
    }

    @Override
    public Optional<T> namedQueryAsSingleResultOptional(String namedQuery, Map<String, Object> params) {
        TypedQuery<T> query = createParametrizedQuery(namedQuery, params);
        T result;
        try {
            result = query.getSingleResult();
        } catch (NoResultException e) {
            return Optional.empty();
        }

        return Optional.of(result);
    }

    private TypedQuery<T> createParametrizedQuery(String namedQuery, Map<String, Object> params) {
        TypedQuery<T> query = entityManager.createNamedQuery(namedQuery, entityType);
        params.forEach(query::setParameter);
        return query;
    }
}

package as.bankapp.enums;

public enum OwnerType {
    PRIVATE,
    CORPORATE
}
